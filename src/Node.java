import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Node class.
 */
public class Node {

    // Only official java documentation and study materials were used to complete the assignment.

    private String name;
    private Node firstChild;
    private Node nextSibling;

    /**
     * Constructor from four double values.
     *
     * @param n name
     * @param d first child
     * @param r next sibling
     */
    Node(String n, Node d, Node r) {
        name = n;
        firstChild = d;
        nextSibling = r;
    }

    /**
     * Parse tree right parenthetic string representation
     *
     * @param s Tree right parenthetic string representation
     * @return Root node
     */
    public static Node parsePostfix(String s) {

        if (s == null) {
            throw new NullPointerException("Input is null");
        }

        if (s.trim().isEmpty()) {
            throw new RuntimeException("Empty string is not valid expression");
        }


        validatePostfixExpression(s);

        //  remove all white spaces
        String expression = s.replaceAll("\\s+", "");


        if (!expression.contains("(")) {
            return new Node(expression, null, null);
        }

        String rootName = getParentName(expression);

        if (rootName.contains(",")) {
            throw new RuntimeException("Invalid coma[s] in " + s + " expression");
        }

        Node root = new Node(rootName, null, null);

        expression = expression.substring(1, expression.length() - rootName.length() - 1);
        parsePostfix(expression, root);

        return root;
    }

    /**
     * Validate tree right parenthetic string representation
     *
     * @param expression Tree right parenthetic string representation
     */
    private static void validatePostfixExpression(String expression) {
        Pattern pattern = Pattern.compile(",{2,}|\\){2,}|(,\\))|(\\),)|(\\).\\()|\\(,|\\(\\)|\\)\\(|([^\\)\\(\\,\\s+]+\\s+[^\\)\\(\\,\\s+]+)");
        Matcher errors = pattern.matcher(expression);
        StringBuilder errorsString = new StringBuilder();
        List<String> errorsList = new ArrayList<>();


        while (errors.find()) {
            errorsList.add(errors.group());
        }
        if (errorsList.size() > 0) {
            for (int i = 0; i < errorsList.size(); i++) {
                errorsString.append("[");
                errorsString.append(i);
                errorsString.append("]");
                errorsString.append("=> ");
                errorsString.append("'");
                errorsString.append(errorsList.get(i));
                errorsString.append("'");
                if (i < errorsList.size() - 1) {
                    errorsString.append(", ");
                }
            }
            throw new RuntimeException(errorsString.toString() + " - invalid part[s] in '" + expression + "' expression");
        }

        Pattern patternNames = Pattern.compile("[^\\)\\(\\,\\s+]");
        Matcher names = patternNames.matcher(expression);

        if (!names.find()) {
            throw new RuntimeException("There are no names in '" + expression + "' expression, only brackets and comas");
        }

        char[] chArr = expression.toCharArray();
        int brackets = 0;
        for (char c : chArr) {
            if ('(' == c) {
                brackets++;
            }
            if (')' == c) {
                brackets--;
            }
            if (',' == c && brackets == 0) {
                throw new RuntimeException("Invalid coma in " + expression + " expression");
            }
        }

        if (!(brackets == 0)) {
            throw new RuntimeException("Invalid brackets in " + expression + " expression");
        }

        if (!expression.contains("(")) {
            if (expression.contains(",")) {
                throw new RuntimeException("Not enough brackets in " + expression + " expression");
            }
        }
    }

    /**
     * Get parent name
     *
     * @param expression Tree right parenthetic string representation
     * @return Parent name
     */
    private static String getParentName(String expression) {
        int index = expression.lastIndexOf(')');
        return expression.substring(index + 1);
    }

    /**
     * Parse tree right parenthetic string representation
     *
     * @param s      Tree right parenthetic string representation
     * @param parent Parent node
     */
    private static void parsePostfix(String s, Node parent) {

        if (s.length() < 1) {
            return;
        }

        if (!s.contains("(")) {
            if (s.contains(",")) {
                StringTokenizer parts = new StringTokenizer(s, ",");

                while (parts.hasMoreTokens()) {
                    String part = parts.nextToken();
                    Node child = new Node(part, null, null);
                    parent.addChild(child);
                }
                return;
            }
            parent.addChild(new Node(s, null, null));
            return;
        }

        char[] chArr = s.toCharArray();
        int bracket = 0;
        int start = 0;
        for (int i = 0; i < chArr.length; i++) {
            if ('(' == chArr[i]) {
                bracket++;
            }

            if (')' == chArr[i]) {
                bracket--;
            }

            if (bracket == 0 && chArr[i] == ',' || i == chArr.length - 1) {
                // todo: refactor
                String nodeName = getParentName(s.substring(start, i == chArr.length - 1 ? i + 1 : i));
                Node node = new Node(nodeName, null, null);
                parent.addChild(node);

                int startChar = start + 1;
                int stopChar = i - nodeName.length() - 1 + (i == chArr.length - 1 ? 1 : 0);

                if (stopChar < startChar) {
                    start = i + 1;
                    continue;
                }

                String expression = s.substring(startChar, stopChar);

                if (expression.length() < 1) {
                    start = i + 1;
                    continue;
                }

                parsePostfix(expression, node);

                start = i + 1;
            }

        }

    }

    /**
     * Create tree left parenthetic string representation
     *
     * @return Tree left parenthetic string representation
     */
    public String leftParentheticRepresentation() {
        StringBuilder expression = new StringBuilder();

        leftParentheticRepresentation(this, expression);

        return expression.toString();
    }

    /**
     * Create tree left parenthetic string representation
     *
     * @param node       Node object
     * @param expression StringBuilder object
     */
    private void leftParentheticRepresentation(Node node, StringBuilder expression) {


        expression.append(node.name);

        if (node.hasChild()) {
            expression.append("(");
            leftParentheticRepresentation(node.getFirstChild(), expression);
        }

        if (node.hasSibling()) {
            expression.append(",");
            leftParentheticRepresentation(node.getNextSibling(), expression);
        } else {
            if (!node.name.equals(name)) {
                expression.append(")");
            }
        }
    }

    /**
     * Validate if this has child
     *
     * @return boolean true/false
     */
    public boolean hasChild() {
        return this.firstChild != null;
    }

    /**
     * Get first child of this
     *
     * @return first child
     */
    public Node getFirstChild() {
        return firstChild;
    }

    /**
     * Add child
     *
     * @param child Child node
     */
    public void addChild(Node child) {
        Node firstChildren = getFirstChild();

        if (firstChildren == null) {
            setFirstChild(child);
        } else {
            while (firstChildren.hasSibling()) {
                firstChildren = firstChildren.getNextSibling();
            }
            firstChildren.setNextSibling(child);
        }
    }


    /**
     * Set first child
     *
     * @param child Child node
     */
    private void setFirstChild(Node child) {
        firstChild = child;
    }

    /**
     * Set next sibling
     *
     * @param sibling Sibling node
     */
    private void setNextSibling(Node sibling) {
        nextSibling = sibling;
    }

    /**
     * Validate if this Node has sibling
     *
     * @return boolean true/false
     */
    public boolean hasSibling() {
        return this.nextSibling != null;
    }

    /**
     * Get next sibling of this node
     *
     * @return next sibling node
     */
    public Node getNextSibling() {
        return nextSibling;
    }


    public static void main(String[] param) {
        String s = "(B1,C)A";
        s = "(   (    (    G,    H  )   D  ,   E , (   I   )   F   )   B   ,   (   J ) C ) A";
//      s = "((D,E,F)B,(G)C)A";
//      s = ",A";
//      s = ".Y.";
//      s = "ABC";
//      s = ")A(";
//        s = "(B)C)(A(";
//        s = ",,,,,))))))))     ,) ),  )f(  (,  ()  )(((";
//        s = "( , ) ";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}

